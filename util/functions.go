package util

func Ternary(cond bool, t interface{}, f interface{}) interface{} {
	if cond {
		return t
	}
	return f
}
