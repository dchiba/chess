package command

import (
	"bitbucket.org/dchiba/chess/ban"
	. "bitbucket.org/dchiba/chess/koma"
	"bitbucket.org/dchiba/chess/player"
	"bitbucket.org/dchiba/chess/teaiwari"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestArrange(t *testing.T) {
	uwate := player.New(1, true)
	shitate := player.New(2, false)
	b := ban.NewDefaultBan()
	tc := teaiwari.Teaiwari{
		teaiwari.Position{4, 0, KING, true},
		teaiwari.Position{4, 7, KING, false},
	}
	Arrange(uwate, shitate, b, tc)

	assert.Equal(t, b.MaxID, uint8(2), "駒が2枚作成されている")

	var k *ban.Koma
	var e error

	k, e = b.GetKoma(&ban.Position{4, 0})
	assert.Nil(t, e, "駒が存在する")
	assert.Equal(t, k.Type, KING, "上手側の玉がある")
	assert.Equal(t, k.Player, uwate, "上手側の玉の所有者が上手")

	k, e = b.GetKoma(&ban.Position{4, 7})
	assert.Nil(t, e, "駒が存在する")
	assert.Equal(t, k.Type, KING, "下手側の玉がある")
	assert.Equal(t, k.Player, shitate, "下手側の玉の所有者が上手")
}

func TestArrange_Hirate(t *testing.T) {
	uwate := player.New(1, true)
	shitate := player.New(2, false)
	b := ban.NewDefaultBan()
	Arrange(uwate, shitate, b, teaiwari.Hirate)

	t.Log(b)

	assert.Equal(t, b.MaxID, uint8(32), "32枚配置されている")

	types := [][]Type{
		{ROOK, KNIGHT, BISHOP, QUEEN, KING, BISHOP, KNIGHT, ROOK},
		{PAWN, PAWN, PAWN, PAWN, PAWN, PAWN, PAWN, PAWN},
		{0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0, 0, 0, 0},
		{PAWN, PAWN, PAWN, PAWN, PAWN, PAWN, PAWN, PAWN},
		{ROOK, KNIGHT, BISHOP, QUEEN, KING, BISHOP, KNIGHT, ROOK},
	}
	for y, row := range types {
		for x, kt := range row {
			k, e := b.GetKoma(&ban.Position{x, y})
			if kt == 0 {
				assert.NotNil(t, e, fmt.Sprintf("X:%d, Y:%d に駒が存在しない", x, y))
			} else {
				assert.Nil(t, e, fmt.Sprintf("X:%d, Y:%d に駒が存在する", x, y))
				assert.Equal(t, k.Type, kt, fmt.Sprintf("X:%d, Y:%d の駒は %s", x, y, kt.Label()))
			}
		}
	}
}

func TestArrange_Mini(t *testing.T) {
	uwate := player.New(1, true)
	shitate := player.New(2, false)
	b := ban.NewMiniBan()
	Arrange(uwate, shitate, b, teaiwari.Mini)

	t.Log(b)

	assert.Equal(t, b.MaxID, uint8(20), "20枚配置されている")

	types := [][]Type{
		{ROOK, KNIGHT, BISHOP, KING, QUEEN},
		{PAWN, PAWN, PAWN, PAWN, PAWN},
		{0, 0, 0, 0, 0},
		{0, 0, 0, 0, 0},
		{PAWN, PAWN, PAWN, PAWN, PAWN},
		{QUEEN, KING, BISHOP, KNIGHT, ROOK},
	}
	for y, row := range types {
		for x, kt := range row {
			k, e := b.GetKoma(&ban.Position{x, y})
			if kt == 0 {
				assert.NotNil(t, e, fmt.Sprintf("X:%d, Y:%d に駒が存在しない", x, y))
			} else {
				assert.Nil(t, e, fmt.Sprintf("X:%d, Y:%d に駒が存在する", x, y))
				assert.Equal(t, k.Type, kt, fmt.Sprintf("X:%d, Y:%d の駒は %s", x, y, kt.Label()))
			}
		}
	}
}

func TestArrange_Micro(t *testing.T) {
	uwate := player.New(1, true)
	shitate := player.New(2, false)
	b := ban.NewMicroBan()
	Arrange(uwate, shitate, b, teaiwari.Micro)

	t.Log(b)

	assert.Equal(t, b.MaxID, uint8(16), "16枚配置されている")

	types := [][]Type{
		{ROOK, QUEEN, KING, ROOK},
		{PAWN, PAWN, PAWN, PAWN},
		{PAWN, PAWN, PAWN, PAWN},
		{ROOK, QUEEN, KING, ROOK},
	}
	for y, row := range types {
		for x, kt := range row {
			k, e := b.GetKoma(&ban.Position{x, y})
			if kt == 0 {
				assert.NotNil(t, e, fmt.Sprintf("X:%d, Y:%d に駒が存在しない", x, y))
			} else {
				assert.Nil(t, e, fmt.Sprintf("X:%d, Y:%d に駒が存在する", x, y))
				assert.Equal(t, k.Type, kt, fmt.Sprintf("X:%d, Y:%d の駒は %s", x, y, kt.Label()))
			}
		}
	}
}
