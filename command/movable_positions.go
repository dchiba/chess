package command

import (
	"bitbucket.org/dchiba/chess/ban"
	"bitbucket.org/dchiba/chess/koma"
	"bitbucket.org/dchiba/chess/player"
	"errors"
	"fmt"
)

var (
	KomaNotFoundError     error = errors.New("Koma is not found")
	KomaNotOwnedError     error = errors.New("Koma is not owned")
	PositionNotFoundError error = errors.New("Posiiton not found")
	TargetKomaOwnedError  error = errors.New("Target Koma is mine")
)

type MoveFunction func(*ban.Position, bool) (*ban.Position, error)

type MovePosition struct {
	From      *ban.Position
	To        *ban.Position
	Nari      bool
	EnPassant bool
	Castling  bool
	Got       bool
}

func NewMovePosition(b *ban.Ban, k *ban.Koma, from *ban.Position, to *ban.Position) *MovePosition {
	p := k.Player
	mp := &MovePosition{from, to, false, false, false, false}
	senteNariHeight := b.NariHeight - 1
	gotenNariHeight := b.Height - b.NariHeight
	if k.Type.CanNari() {
		if p.Sente {
			if from.Y <= senteNariHeight || to.Y <= senteNariHeight {
				mp.Nari = true
			}
		} else {
			if from.Y >= gotenNariHeight || to.Y >= gotenNariHeight {
				mp.Nari = true
			}
		}
	}
	return mp
}

func (p *MovePosition) String() string {
	return fmt.Sprintf("{FROM:%v, TO:%v, N:%t}", p.From, p.To, p.Nari)
}

type MovePositions []*MovePosition

func (ps MovePositions) Contain(q *ban.Position) (b bool) {
	b = false
	for _, p := range ps {
		if p.To.Equal(q) {
			b = true
			break
		}
	}
	return
}

func MovablePositions(p *player.Player, b *ban.Ban, pos *ban.Position) (MovePositions, error) {
	k, e := b.GetKoma(pos)
	if e != nil {
		return nil, KomaNotFoundError
	}
	if k.Player.ID != p.ID {
		return nil, KomaNotOwnedError
	}

	positions := MovePositions{}

	for _, m := range k.GetMoveTypes() {
		var ps MovePositions
		var e error

		switch m {
		case koma.Front:
			ps, e = FrontPositions(b, k, pos)
		case koma.Back:
			ps, e = BackPositions(b, k, pos)
		case koma.Right:
			ps, e = RightPositions(b, k, pos)
		case koma.Left:
			ps, e = LeftPositions(b, k, pos)
		case koma.FrontRight:
			ps, e = FrontRightPositions(b, k, pos)
		case koma.FrontLeft:
			ps, e = FrontLeftPositions(b, k, pos)
		case koma.BackRight:
			ps, e = BackRightPositions(b, k, pos)
		case koma.BackLeft:
			ps, e = BackLeftPositions(b, k, pos)
		case koma.FrontStraight:
			ps, e = FrontStraightPositions(b, k, pos)
		case koma.BackStraight:
			ps, e = BackStraightPositions(b, k, pos)
		case koma.RightStraight:
			ps, e = RightStraightPositions(b, k, pos)
		case koma.LeftStraight:
			ps, e = LeftStraightPositions(b, k, pos)
		case koma.FrontRightStraight:
			ps, e = FrontRightStraightPositions(b, k, pos)
		case koma.FrontLeftStraight:
			ps, e = FrontLeftLeftStraightPositions(b, k, pos)
		case koma.BackRightStraight:
			ps, e = BackRightStraightPositions(b, k, pos)
		case koma.BackLeftStraight:
			ps, e = BackLeftStraightPositions(b, k, pos)
		case koma.FrontFrontRightJump:
			ps, e = FrontFrontRightJumpPositions(b, k, pos)
		case koma.FrontFrontLeftJump:
			ps, e = FrontFrontLeftJumpPositions(b, k, pos)
		case koma.FrontRightRightJump:
			ps, e = FrontRightRightJumpPositions(b, k, pos)
		case koma.FrontLeftLeftJump:
			ps, e = FrontLeftLeftJumpPositions(b, k, pos)
		case koma.BackRightRightJump:
			ps, e = BackRightRightJumpPositions(b, k, pos)
		case koma.BackLeftLeftJump:
			ps, e = BackLeftLeftJumpPositions(b, k, pos)
		case koma.BackBackRightJump:
			ps, e = BackBackRightJumpPositions(b, k, pos)
		case koma.BackBackLeftJump:
			ps, e = BackBackLeftJumpPositions(b, k, pos)
		case koma.Pawn:
			ps, e = PawnPositions(b, k, pos)
		case koma.Castling:
			ps, e = CastlingPositions(b, k, pos)
		}

		if e == nil {
			positions = append(positions, ps...)
		}
	}

	return positions, nil
}

func FrontPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Front)
}

func BackPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Back)
}

func RightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Right)
}

func LeftPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Left)
}

func FrontRightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.FrontRight)
}

func FrontLeftPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.FrontLeft)
}

func BackRightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.BackRight)
}

func BackLeftPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.BackLeft)
}

func FrontStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Front, b.Front, b.Front, b.Front, b.Front, b.Front, b.Front, b.Front)
}

func BackStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Back, b.Back, b.Back, b.Back, b.Back, b.Back, b.Back, b.Back)
}

func RightStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Right, b.Right, b.Right, b.Right, b.Right, b.Right, b.Right, b.Right)
}

func LeftStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.Left, b.Left, b.Left, b.Left, b.Left, b.Left, b.Left, b.Left)
}

func FrontRightStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.FrontRight, b.FrontRight, b.FrontRight, b.FrontRight, b.FrontRight, b.FrontRight, b.FrontRight, b.FrontRight)
}

func FrontLeftLeftStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.FrontLeft, b.FrontLeft, b.FrontLeft, b.FrontLeft, b.FrontLeft, b.FrontLeft, b.FrontLeft, b.FrontLeft)
}

func BackRightStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.BackRight, b.BackRight, b.BackRight, b.BackRight, b.BackRight, b.BackRight, b.BackRight, b.BackRight)
}

func BackLeftStraightPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, false, b.BackLeft, b.BackLeft, b.BackLeft, b.BackLeft, b.BackLeft, b.BackLeft, b.BackLeft, b.BackLeft)
}

func FrontFrontRightJumpPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, true, b.Front, b.Front, b.Right)
}

func FrontFrontLeftJumpPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, true, b.Front, b.Front, b.Left)
}

func FrontRightRightJumpPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, true, b.Front, b.Right, b.Right)
}

func FrontLeftLeftJumpPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, true, b.Front, b.Left, b.Left)
}

func BackRightRightJumpPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, true, b.Back, b.Right, b.Right)
}

func BackLeftLeftJumpPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, true, b.Back, b.Left, b.Left)
}

func BackBackRightJumpPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, true, b.Back, b.Back, b.Right)
}

func BackBackLeftJumpPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	return GetPositions(b, k, pos, true, b.Back, b.Back, b.Left)
}

func GetPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position, jump bool, funcs ...MoveFunction) (MovePositions, error) {
	positions := MovePositions{}
	before := pos
	last := len(funcs) - 1
	for i, f := range funcs {
		dest, e := f(before, k.Player.Sente)
		if e != nil {
			// Position が取得できない
			break
		}
		before = dest

		// ジャンプの途中なら次の移動へ
		if jump && i != last {
			continue
		}

		target, e := b.GetKoma(dest)
		if e == nil {
			// 駒がある
			if target.Player.ID != k.Player.ID {
				// 相手の駒
				mp := NewMovePosition(b, k, pos, dest)
				mp.Got = true
				positions = append(positions, mp)
			}
			break
		}
		positions = append(positions, NewMovePosition(b, k, pos, dest))
	}
	if len(positions) <= 0 {
		return nil, PositionNotFoundError
	}
	return positions, nil
}

func PawnPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	positions := MovePositions{}
	// 前に他のコマがなければすすめる
	if d1, e := b.Front(pos, k.Player.Sente); e == nil {

		if k1, _ := b.GetKoma(d1); k1 == nil {
			positions = append(positions, NewMovePosition(b, k, pos, d1))
			// コマが動いておらず前に他のコマがなければさらに前にすすめる
			if k.MoveCount == 0 {
				if d2, e := b.Front(d1, k.Player.Sente); e == nil {
					if k2, _ := b.GetKoma(d2); k2 == nil {
						positions = append(positions, NewMovePosition(b, k, pos, d2))
					}
				}
			}
		}
		// 斜め前に敵の駒があればとってすすめる
		if d3, e := b.Right(d1, k.Player.Sente); e == nil {
			if k3, _ := b.GetKoma(d3); k3 != nil && k3.Player.ID != k.Player.ID {
				mp := NewMovePosition(b, k, pos, d3)
				mp.Got = true
				positions = append(positions, mp)
			}
		}
		if d4, e := b.Left(d1, k.Player.Sente); e == nil {
			if k4, _ := b.GetKoma(d4); k4 != nil && k4.Player.ID != k.Player.ID {
				mp := NewMovePosition(b, k, pos, d4)
				mp.Got = true
				positions = append(positions, mp)
			}
		}
	}
	// 敵のポーンが初手で2マス進んだ時に隣りにいたら斜め前に進みながら取ることができる（アンパッサン）
	if (k.Player.Sente && pos.Y == 3) || (!k.Player.Sente && pos.Y == 4) {
		if d5, e := b.Right(pos, k.Player.Sente); e == nil {
			if k5, _ := b.GetKoma(d5); k5 != nil && k5.Player.ID != k.Player.ID && k5.MoveCount == 1 {
				if d5_2, e := b.Front(d5, k.Player.Sente); e == nil {
					mp := NewMovePosition(b, k, pos, d5_2)
					mp.EnPassant = true
					mp.Got = true
					positions = append(positions, mp)
				}
			}
		}
		if d6, e := b.Left(pos, k.Player.Sente); e == nil {
			if k6, _ := b.GetKoma(d6); k6 != nil && k6.Player.ID != k.Player.ID && k6.MoveCount == 1 {
				if d6_2, e := b.Front(d6, k.Player.Sente); e == nil {
					mp := NewMovePosition(b, k, pos, d6_2)
					mp.EnPassant = true
					mp.Got = true
					positions = append(positions, mp)
				}
			}
		}
	}
	// ポジションの返却
	if len(positions) <= 0 {
		return nil, PositionNotFoundError
	}
	return positions, nil
}

func CastlingPositions(b *ban.Ban, k *ban.Koma, pos *ban.Position) (MovePositions, error) {
	if k.Type != koma.KING || k.MoveCount != 0 || b.Width != ban.DefaultWidth {
		return nil, PositionNotFoundError
	}
	positions := MovePositions{}
	if canCastling(b, &ban.Position{0, pos.Y}, pos) {
		mp := NewMovePosition(b, k, pos, &ban.Position{2, pos.Y})
		mp.Castling = true
		positions = append(positions, mp)
	}
	if canCastling(b, pos, &ban.Position{7, pos.Y}) {
		mp := NewMovePosition(b, k, pos, &ban.Position{6, pos.Y})
		mp.Castling = true
		positions = append(positions, mp)
	}
	return positions, nil
}

func canCastling(b *ban.Ban, pos1 *ban.Position, pos2 *ban.Position) bool {
	k1, _ := b.GetKoma(pos1)
	k2, _ := b.GetKoma(pos2)
	if k1 == nil || k2 == nil || k1.MoveCount != 0 || k2.MoveCount != 0 {
		return false
	}
	for i := pos1.X + 1; i < pos2.X; i++ {
		if k, _ := b.GetKoma(&ban.Position{i, pos1.Y}); k != nil {
			return false
		}
	}
	return true
}
