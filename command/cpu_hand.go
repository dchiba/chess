package command

import (
	"bitbucket.org/dchiba/chess/ban"
	"bitbucket.org/dchiba/chess/player"
	"errors"
	"math/rand"
	"time"
)

func CpuHand(p *player.Player, b *ban.Ban, level int) (mp *MovePosition, moved *ban.Koma, got *ban.Koma, castled *ban.Koma, e error) {

	// 全ての駒を検索して自分の駒で動かせるもののリストをつくる
	movePositions := MovePositions{}
	kikiMap := map[ban.Position]bool{}
	for x := 0; x < b.Width; x++ {
		for y := 0; y < b.Height; y++ {
			pos := &ban.Position{x, y}
			k, e := b.GetKoma(pos)
			if e != nil || k == nil || !k.IsMovableTime() {
				continue
			}
			if k.Player.ID != p.ID {
				mps, e := MovablePositions(k.Player, b, pos)
				if e != nil {
					continue
				}
				for _, mp := range mps {
					kikiMap[*(mp.To)] = true
				}
				continue
			}
			// MovePositions を使って動かせられる手を検索する
			ps, e := MovablePositions(p, b, pos)
			if e != nil {
				continue
			}
			movePositions = append(movePositions, ps...)
		}
	}

	if len(movePositions) == 0 {
		return nil, nil, nil, nil, errors.New("not found")
	}

	// 移動先の抽選箱をつくる
	mpMap := map[*MovePosition]int{}
	total := 0
	for _, movePosition := range movePositions {
		if movePosition.Got {
			// 相手の駒を取れる
			mpMap[movePosition] = 500
		} else if _, ok := kikiMap[*(movePosition.To)]; ok {
			// 相手に取られる
			mpMap[movePosition] = 5
		} else {
			// その他
			mpMap[movePosition] = 50
		}
		total += mpMap[movePosition]
	}

	// ランダムに動かす手を抽選
	rand.Seed(time.Now().UnixNano())
	if level == 0 {
		// Easy
		mp = movePositions[rand.Intn(len(movePositions))]
	} else {
		// Normal or Hard
		// 手の中から優先度に従った重み付きランダムで実際に動かす手を抽選
		n := rand.Intn(total)
		for k, v := range mpMap {
			n -= v
			if n < 0 {
				mp = k
				break
			}
		}
	}

	// Move を使って動かす
	moved, got, castled, e = Move(p, b, mp.From, mp.To, mp.Nari)
	return mp, moved, got, castled, e
}
