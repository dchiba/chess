package command

import (
	"bitbucket.org/dchiba/chess/apptime"
	"bitbucket.org/dchiba/chess/ban"
	"bitbucket.org/dchiba/chess/koma"
	"bitbucket.org/dchiba/chess/player"
	"bitbucket.org/dchiba/chess/teaiwari"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestCpuHand(t *testing.T) {
	assert := assert.New(t)
	b := ban.NewDefaultBan()
	p1 := player.New(1, true)
	p2 := player.New(2, false)

	Arrange(p1, p2, b, teaiwari.Teaiwari{
		teaiwari.Position{4, 3, koma.PAWN, true},
		teaiwari.Position{4, 1, koma.PAWN, false},
	})

	var mp *MovePosition
	var moved *ban.Koma
	var got *ban.Koma
	var e error

	mp, moved, got, _, e = CpuHand(p1, b, 0)
	assert.NotNil(e, "移動できない")

	apptime.SetTime(time.Now().Add(1 * time.Minute)) // 駒の移動待ち時間のために時間をすすめる
	mp, moved, got, _, e = CpuHand(p1, b, 0)
	assert.Nil(e, "移動できる")
	assert.NotNil(moved, "移動した駒が取得できる")
	assert.Nil(got, "駒は獲得しない")
	assert.Equal(mp.From, &ban.Position{4, 3}, "移動元が正常")
	assert.Equal(mp.To, &ban.Position{4, 2}, "移動先が正常")
}

func TestCpuHand2(t *testing.T) {
	assert := assert.New(t)
	b := ban.NewDefaultBan()
	p1 := player.New(1, true)
	p2 := player.New(2, false)

	Arrange(p1, p2, b, teaiwari.Teaiwari{
		teaiwari.Position{4, 2, koma.KING, true},
		teaiwari.Position{4, 1, koma.PAWN, false},
	})

	apptime.SetTime(time.Now().Add(2 * time.Minute)) // 駒の移動待ち時間のために時間をすすめる
	mp, _, _, _, e := CpuHand(p1, b, 1)
	assert.Nil(e, "移動できる")
	x := mp.To.X
	y := mp.To.Y
	assert.True(3 <= x && x <= 5 && 1 <= y && y <= 3, "周囲1マスに移動できる")

	t.Logf("%v", mp)
}
