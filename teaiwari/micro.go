package teaiwari

import (
	"bitbucket.org/dchiba/chess/koma"
)

var Micro Teaiwari = Teaiwari{
	Position{0, 0, koma.ROOK, true},
	Position{1, 0, koma.QUEEN, true},
	Position{2, 0, koma.KING, true},
	Position{3, 0, koma.ROOK, true},

	Position{0, 1, koma.PAWN, true},
	Position{1, 1, koma.PAWN, true},
	Position{2, 1, koma.PAWN, true},
	Position{3, 1, koma.PAWN, true},

	Position{0, 2, koma.PAWN, false},
	Position{1, 2, koma.PAWN, false},
	Position{2, 2, koma.PAWN, false},
	Position{3, 2, koma.PAWN, false},

	Position{0, 3, koma.ROOK, false},
	Position{1, 3, koma.QUEEN, false},
	Position{2, 3, koma.KING, false},
	Position{3, 3, koma.ROOK, false},
}
