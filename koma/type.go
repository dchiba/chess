package koma

import "errors"

type Type uint8

const (
	_ Type = iota
	KING
	QUEEN
	ROOK
	BISHOP
	KNIGHT
	PAWN
)

var NariMap map[Type]Type = map[Type]Type{
	PAWN: QUEEN,
}

var LabelList = [...]string{
	"",
	"K", "Q", "R", "B", "K", "P",
}

var WaitTimeList = [...]int64{
	0,  // _ Type = iota
	10, // KING
	10, // QUEEN
	7,  // ROOK
	7,  // BISHOP
	5,  // KNIGHT
	3,  // PAWN
}

func (t Type) CanNari() bool {
	_, ok := NariMap[t]
	return ok
}

func (t Type) Nari() (rt Type, e error) {
	rt, ok := NariMap[t]
	if !ok {
		e = errors.New("invalid type")
	}
	return
}

func (t Type) Label() string {
	return LabelList[t]
}
