package koma

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestType_CanNari(t *testing.T) {
	samples := []struct {
		Type         Type
		ExpectedBool bool
	}{
		{KING, false}, {QUEEN, false}, {ROOK, false}, {BISHOP, false}, {KNIGHT, false}, {PAWN, true},
	}
	for _, s := range samples {
		assert.Equal(t, s.ExpectedBool, s.Type.CanNari())
	}
}
