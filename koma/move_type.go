package koma

type MoveType uint8

const (
	_ MoveType = iota
	Front
	Back
	Right
	Left
	FrontRight
	FrontLeft
	BackRight
	BackLeft
	FrontStraight
	BackStraight
	RightStraight
	LeftStraight
	FrontRightStraight
	FrontLeftStraight
	BackRightStraight
	BackLeftStraight
	FrontFrontRightJump
	FrontFrontLeftJump
	BackBackRightJump
	BackBackLeftJump
	FrontRightRightJump
	FrontLeftLeftJump
	BackRightRightJump
	BackLeftLeftJump
	Pawn
	Castling
)

type MoveTypes []MoveType

var KomaMoveType map[Type]MoveTypes = map[Type]MoveTypes{
	KING:   MoveTypes{Front, Back, Right, Left, FrontRight, FrontLeft, BackRight, BackLeft, Castling},
	QUEEN:  MoveTypes{FrontStraight, BackStraight, RightStraight, LeftStraight, FrontRightStraight, FrontLeftStraight, BackRightStraight, BackLeftStraight},
	ROOK:   MoveTypes{FrontStraight, BackStraight, RightStraight, LeftStraight},
	BISHOP: MoveTypes{FrontRightStraight, FrontLeftStraight, BackRightStraight, BackLeftStraight},
	KNIGHT: MoveTypes{FrontFrontRightJump, FrontFrontLeftJump, BackBackRightJump, BackBackLeftJump, BackRightRightJump, BackLeftLeftJump, FrontRightRightJump, FrontLeftLeftJump},
	PAWN:   MoveTypes{Pawn},
}
