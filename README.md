### Install
```
$ go get github.com/Masterminds/glide
$ go install github.com/Masterminds/glide
$ glide install
```

### Test
```
$ go test $(go list ./... | grep -v /vendor/)
```

### Settings for Intellij IDEA
* Select Menu `Intellij IDEA > Preferences > Languages & Frameworks > Go > Project Settings`
* Change "Enable vendoring" to "Enabled"